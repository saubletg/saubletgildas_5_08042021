import { createHomepage } from "/src/homepage.js";
import { createPhotographerPage } from "/src/photographer-page.js";
import { linkContent } from "/src/link-content.js"


// Create page based on the pathname
// Select the path element
const path = window.location.pathname;

// Create the photographer page
if (path.includes("photographer.html"))
createPhotographerPage();

//Create the index Homepage
else {
  createHomepage();
  linkContent();
}